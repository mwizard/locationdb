# README #


### What is this repository for? ###

* This is a repository to hold the sql scripts necessay to create the tables to store location information. This includes 4 tables and 6 stored procedures.

  
Tables  
 Locations-Stores the actual locations with foreign key to Neighborhoods table.  
 Neighborhoods-Stores Neighborhoods.  
 Type-Stores the different types of locations.  
 TypeLocations-Table to link Type and Locations tables together. This allows a Location to have more than one type.  
  

Stored Procedures  
 Add_Type-Add types to a location  
 CreateLocations-Takes in Longitude,Latitude,Name,Neighborhood,Description and inserts into Locations table.  
 Get_ByName-Gets Location based on Name.  
 Get_ByNeighborhood-Gets Locations based upon neighborhood.  
 Get_ByPoint-Gets location based upon Longitude,Latitude  
 Get_ByType-Gets locations by type.  
 


### How do I get set up? ###

* Run the scripts on a database of your choice. Make sure to replace BaltimoreLocations as db name if yours is named something different.