USE [BaltimoreLocations]
GO

/****** Object:  Table [dbo].[Locations]    Script Date: 9/25/2016 3:25:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Locations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Location] [geography] NULL,
	[Name] [varchar](255) NULL,
	[Neighborhood] [int] NULL,
	[Description] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Locations]  WITH CHECK ADD FOREIGN KEY([Neighborhood])
REFERENCES [dbo].[Neighborhoods] ([ID])
GO


