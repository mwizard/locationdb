USE [BaltimoreLocations]
GO

/****** Object:  StoredProcedure [dbo].[CreateLocation]    Script Date: 9/25/2016 3:30:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Mark Wisser>
-- Create date: <9/24/2016>
-- Description:	<Creates a new location>
-- =============================================
CREATE PROCEDURE [dbo].[CreateLocation] 
	-- Add the parameters for the stored procedure here
	@Latitude float,
	@Longitude float,
	@Name varchar(255),
	@Neighborhood varchar(255),
	@Description varchar(255)
AS
BEGIN TRANSACTION
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Get Neighborhood number
	DECLARE @Neighborhood_NUmber int;
	SET @Neighborhood_Number = (SELECT ID FROM Neighborhoods WHERE Neighborhoods.Name = @Neighborhood);

	
	IF @@ERROR <> 0
		BEGIN
		ROLLBACK TRANSACTION
		return 10
		END
	
	--Insert

	INSERT INTO Locations
	VALUES(geography::Point(@Latitude, @Longitude, 4326),@Name,@Neighborhood_Number,@Description);

    IF @@ERROR <> 0
		BEGIN
		ROLLBACK TRANSACTION
		return 11
		END
COMMIT TRANSACTION


GO


