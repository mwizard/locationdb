USE [BaltimoreLocations]
GO

/****** Object:  StoredProcedure [dbo].[Get_ByPoint]    Script Date: 9/25/2016 3:45:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Mark Wisser>
-- Create date: <9/24/2016>
-- Description:	<Get Location by Point>
-- =============================================
CREATE PROCEDURE [dbo].[Get_ByPoint] 
	-- Add the parameters for the stored procedure here
	@Longitude float, 
	@Latitude float
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Point geography;
	SET @Point=geography::Point(@Longitude,@Latitude,4326);
    -- Insert statements for procedure here
	SELECT * FROM Locations
	WHERE @Point.STEquals(Location)=1;

END

GO


