USE [BaltimoreLocations]
GO

/****** Object:  StoredProcedure [dbo].[Get_ByType]    Script Date: 9/25/2016 3:45:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Mark Wisser>
-- Create date: <9/23/2016>
-- Description:	<Returns Locations based on type. >
-- =============================================
CREATE PROCEDURE [dbo].[Get_ByType]
	-- Add the parameters for the stored procedure here
	@Type varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM Locations 
	INNER JOIN TypeLocations 
	ON Locations.ID=TypeLocations.LocationID
	INNER JOIN Type
	ON Type.ID=TypeLocations.TypeID
	WHERE @Type = Type.Name;

END

GO


