USE [BaltimoreLocations]
GO

/****** Object:  StoredProcedure [dbo].[Add_Type]    Script Date: 9/25/2016 3:29:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Mark Wisser>
-- Create date: <9/25/2016>
-- Description:	<Add a type to>
-- =============================================
CREATE PROCEDURE [dbo].[Add_Type] 
	-- Add the parameters for the stored procedure here
	@Location varchar(255), 
	@Type varchar(255)
AS
BEGIN TRANSACTION
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @TypeId int;
	DECLARE @LocationId int;

	SET @TypeId=(SELECT ID FROM TYPE WHERE Type.Name=@Type);
	SET @LocationId=(SELECT ID FROM Locations WHERE Locations.Name=@Location);

	IF EXISTS(SELECT * FROM TypeLocations WHERE TypeId=@TypeId AND LocationId=@LocationId)
		BEGIN
		ROLLBACK TRANSACTION
		return 11
		END

	INSERT INTO TypeLocations
	VALUES(@TypeID,@LocationID)

	IF @@ERROR <> 0
		BEGIN
		ROLLBACK TRANSACTION
		return 10
		END
COMMIT TRANSACTION

GO


