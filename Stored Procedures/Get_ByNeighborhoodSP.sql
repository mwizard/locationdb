USE [BaltimoreLocations]
GO

/****** Object:  StoredProcedure [dbo].[Get_ByNeighborhood]    Script Date: 9/25/2016 3:44:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Mark Wisser>
-- Create date: <9/24/2016>
-- Description:	<Get Locations by neighborhood>
-- =============================================
CREATE PROCEDURE [dbo].[Get_ByNeighborhood] 
	-- Add the parameters for the stored procedure here
	@Name varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Locations
	INNER JOIN Neighborhoods
	ON Locations.Neighborhood=Neighborhoods.ID
	WHERE Neighborhoods.Name = @Name
END

GO


