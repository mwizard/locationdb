USE [BaltimoreLocations]
GO

/****** Object:  StoredProcedure [dbo].[Get_ByName]    Script Date: 9/25/2016 3:44:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Mark Wisser>
-- Create date: <9/24/2016>
-- Description:	<Get Location by Name>
-- =============================================
CREATE PROCEDURE [dbo].[Get_ByName]
	
	@Name varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Locations 
	WHERE Locations.Name = @Name
END

GO


